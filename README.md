# Recustom QA Challenge

In order to complete the challenge, you will need to do the following
- Write a test case for the below user story
- Commit the manual test cases to this repo
- Share this repo with saalihou@gmail.com

### Optional (but appreciated in order to show your test automation experience)
- Automate the test case using your preferred web automation tool
- Commit the automated test case script in this repo

# User Story - Search for Haggadah and Share
As a user I want to be able to search and share my favorite haggadahs so I can spread the word around jewish rituals

**Acceptance Criteria**
- On haggadot.com, I can search for a Haggadah next to my favorite haggadahs using a keyword
- When I do so, a list of haggadahs matching the keyword are presented
- I can share a specific haggadah from the results
- When I share the haggadah, a link is presentend to me that I can copy for sharing purposes
- I can also share the haggadah as a post on Facebook, Twitter or LinkedIn
